function setup() {
  createCanvas(800, 800, WEBGL);
  rectMode(CENTER);
}

function draw() {
  background(190);
  rotate(radians(frameCount));
  rect(0, 0, 100, 100);
}




let x=0;
let y=0;

function setup() {
  createCanvas(800, 800, WEBGL);
}

function draw() {
  background(190);
  ellipse(x, y, 70, 70);
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
      y = y-10;
    } else if (keyCode === DOWN_ARROW) {
      y = y+10;
    } else if (keyCode === RIGHT_ARROW) {
      x = x+10;
    } else if (keyCode === LEFT_ARROW) {
      x = x-10;
    }
  }
  
  
  
  
  
  let message = 'ok'

function setup() {
  createCanvas(600, 300);
  background(220);
  
  createInput(message).input(e => {message = e.target.value})
}

function draw(){
  translate(width/2, height/2)
  rotate(frameCount)
  text(message,0,0)
}





function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);
  text('x:'+ mouseX +' / y:'+mouseY, 10,10);
}




function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(255);
  drawBoard();
  
}

function drawBoard() {
  black = 220;
  white = 30;
  for (y = 0; y < height; y += 50) {
    for (x = 0; x < width; x += 50) {
      if (x % 100 == 0) {
        if (y % 100 == 0) {
          fill(black);
        }
        if (y % 100 == 50) {
          fill(white);
        }
      }
      if (x % 100 == 50) {
        if (y % 100 == 50) {
          fill(black);
        }
        if (y % 100 == 0) {
          fill(white);
        }
      }

      rect(x, y, 50, 50);
    }
  }
}






function setup() {
  createCanvas(400, 400);
}

function draw() {
  if (mouseY > height/2) {
    background('red');
  }
  else {
    background('white')
  }
  line(0, height/2,width, width/2)
}






var x = 0;
var y = 200;
var speedX = 3;
var speedY = 4;


var x2 = 200;
var y2 = 100;
var speedX2 = 1;
var speedY2 = 5;

function setup() {
  createCanvas(600, 400);
}

function draw() {
  background(220);
  ellipse(x, 200, 40, 40);
  ellipse(x2, 200, 90, 90);
  
  x += speedX;

  if (x > width - 20 || x < 0 || x > x2 - 65) {
    speedX *= -1;
  }
  
  x2 += speedX2;

  if (x2 > width - 45 || x2 < 50) {
    speedX2 *= -1;
  }
}




function setup() {
  createCanvas(800, 800,WEBGL);
  rectMode(CENTER)
}

function draw() {
  background(190);
  push()
  rotate(-radians(frameCount));
  rect(100, 100, 100, 100);
  pop()
  
  push()
  rotate(radians(frameCount));
  rect(200, 200, 100, 100);
  pop()
}




upper = 1.85 // upper mouth radian
lower = 0.2 // lower mouth radian
speed = 0.01

function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(0);
  
  fill(255, 255, 0);
  ellipse(200, 200, 200, 200);
  
  fill(0);
  if (upper >= 2 || upper <= 1.8) {
    speed = speed*(-1) 
  }
  arc(200, 200, 200, 200, 
      (upper+=speed)*PI, (lower-=speed)*PI, PIE);
  
  //Pacman eye
  ellipse(200, 150, 15, 15);
}




let sliderVolume;
let sliderPan;
let button
let mp3;
let amplitudes = [];

function preload() {
  mp3 = loadSound('test.mp3');
}

function setup() {
  createCanvas(400, 400);
  sliderVolume = createSlider(0, 1, 0.5, 0.1);
  sliderPan = createSlider(-1, 1, 0, 0.1);
  button = createButton('Play/Stop');
  button.mousePressed(PlayStop);
  amplitude = new p5.Amplitude()
}

function PlayStop() {
  if (mp3.isPlaying()) {
    mp3.pause();
  } else {
    mp3.play();
  }
}

function draw() {
  mp3.setVolume(sliderVolume.value())
  mp3.pan(sliderPan.value())
  amplitudes.push(map(amplitude.getLevel(), 0, 1, 1, height))
  if (amplitudes.length > width) {
    amplitudes.splice(0, 1);
  }
  amplitudes.forEach((amp, i) => {
    line(i, 0, i, amp)
  })
}